let alumnos = [
  {
    nombre: 'Gonzalo Quispe Chumacero',
    sexo: 'masculino',
    edad: 29,
    matricula: 1,
    curso: {
      nombre: 'Programacion III',
      semestre: 'Tercero',
    },
  },
  {
    nombre: 'Fernanda Lopez',
    sexo: 'femenino',
    edad: 22,
    matricula: 2,
    curso: {
      nombre: 'Programacion I',
      semestre: 1,
    },
  },
  {
    nombre: 'Wayne Rooney',
    sexo: 'masculino',
    edad: 25,
    matricula: 3,
    curso: {
      nombre: 'Estadistica',
      semestre: 5,
    },
  },
];

const CANTIDAD_ESTUDIANTES = alumnos.length;
let cantHombres = 0;
let cantMujeres = 0;
let estudiantes_20_25 = 0;

alumnos.forEach((est) => {
  if (est.sexo === 'masculino') {
    cantHombres += 1;
  } else {
    cantMujeres += 1;
  }
  if (est.edad >= 20 && est.edad <= 25) {
    estudiantes_20_25 += 1;
  }
});

alumnos.sort((est1, est2) => est1.nombre.localeCompare(est2.nombre));

console.log('Listado de alumnos:');
alumnos.forEach(({ nombre, curso }) => {
  console.log(`${nombre}, curso: ${curso.nombre}, semestre: ${curso.semestre}`);
});

console.log('Estadisticas:');
console.log(`Hombres: ${cantHombres}; Mujeres: ${cantMujeres}`);
console.log(
  `Personas entre 20 - 25: ${
    Math.round((estudiantes_20_25 / CANTIDAD_ESTUDIANTES) * 10000) / 100
  }`
);
